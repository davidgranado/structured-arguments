import { ActivityTypes } from './constants';
import type { Auth } from '@firebase/auth';
import type { FirebaseApp } from '@firebase/app';

export
interface NewPostParentResponse {
	comment?: string;
	parentPostId: string;
}

export
interface NewPost {
	body: string;
	title: string;
	parentPostResponses: NewPostParentResponse[];
}

export
interface Post extends PostCommon {
	_id?: string;
	bookmarked?: boolean;
}

export
interface PostCommon {
	body: string;
	childPostIds: string[];
	created: string;
	lastUpdated: string;
	ownerId?: string;
	points: number;
	parentPostResponses: PostResponse[];
	title: string;
}

export
type PostUpdate = Pick<Post, 'body' | 'title'>;

export
interface PostResponse {
	comment?: string;
	created: string;
	parentPostId: string;
	points: number;
	userId?: string;
	lastUpdated: string;
}

export
interface State {
	app: FirebaseApp | null;
	auth: Auth | null;
	loading: boolean;
	sideMenuOpen?: boolean;
	activeBoostPost: Post | null;
	user: null | {
		email: string;
		id: string;
		pointBalance: number;
	};
	pointSpendPrompt: null | {
		points: number;
		isSelfSpend?: boolean;
		onConfirm(): void;
		onCancel(): void;
	};
	toastMsg: string;
	feed: Post[];
}

export
interface UserActivity {
	date: string;
	userId: string;
	type: ActivityTypes;
	params?: any;
}
