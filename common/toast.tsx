import { useStore } from '@common/store';
import { useEffect, useState } from 'react';
import { setToastMsg } from './store-actions';
import { Snackbar, IconButton } from '@mui/material';
import { CloseIcon } from '@components/icons';

export
function Toast() {
	const toastMsg = useStore(s => s.toastMsg);
	const [isOpen, setOpen] = useState(false);

	useEffect(() => {
		setOpen(!!toastMsg);
	}, [toastMsg]);

	function handleClose() {
		setOpen(false);
	}

	return (
		<>
			<Snackbar
				open={isOpen}
				autoHideDuration={4000}
				onClose={handleClose}
				TransitionProps={{ onExited: () => setToastMsg('') }}
				message={toastMsg}
				action={
					<IconButton
						size="small"
						color="inherit"
						onClick={handleClose}
					>
						<CloseIcon fontSize="small" />
					</IconButton>
				}
			/>
		</>
	);
}
