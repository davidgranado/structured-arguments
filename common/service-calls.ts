import { API_URL, MIN_POST_POINTS } from '@common/constants';
import {
	get,
	post,
	urlJoin,
} from '@common/utils';
import type {
	NewPost,
	Post,
	PostUpdate,
} from '@common/types';

export
function editPost(id: string, postUpdate: PostUpdate) {
	return apiPost(`post/${id}/update`, { postUpdate });
}

export
function boostPost(postId: string, points: number, commentUserId?: string, parentPostId?: string) {
	const comment = (commentUserId && parentPostId) ? {
		commentUserId,
		parentPostId,
	} : undefined;
	return apiPost(`boost-post/${postId}`, {
		points,
		comment,
	});
}

export
async function getPointBalance() {
	const response = await apiGet('/points');

	return response?.data.points;
}

export
function createPost(newPost: NewPost, points: number) {
	if(points < MIN_POST_POINTS) {
		throw `Must spend at least ${MIN_POST_POINTS} points.`;
	}

	return apiPost('post', {
		points,
		post: newPost,
	});
}

export
async function getPost(id: string): Promise<Post> {
	const { post: p } = await apiGet(`post/${id}`);

	return p;
}

export
async function bookmarkPost(id: string) {
	return apiPost(`post/${id}/bookmark`);
}

export
async function unbookmarkPost(id: string) {
	return apiPost(`post/${id}/remove-bookmark`);
}

export
function connectPost(postId: string, parentPostId: string, points: number, comment = '') {
	return apiPost(`post/${postId}/response/${parentPostId}/create`, {
		comment,
		points,
	});
}

function apiPost(path: string, requestBody?: any) {
	return post(urlJoin(API_URL, path), requestBody);
}

function apiGet(path: string, params?: any) {
	return get(urlJoin(API_URL, path), params);
}
