import { init } from 'next-firebase-auth';
import { firebasePrivateConfig } from '../firebase-config';
import { COOKIE_NAME, IS_DEV } from '@common/constants';
import { IncomingMessage } from 'http';

const COOKIE_SECRET = process.env.COOKIE_SECRET;

const {
	apiKey,
	authDomain,
	projectId,
	privateKey,
	clientEmail,
} = firebasePrivateConfig;

export
function initAuth() {
	console.log('initAuth');
	init({
		authPageURL: '/auth',
		appPageURL: '/',
		loginAPIEndpoint: '/api/login', // required
		logoutAPIEndpoint: '/api/logout', // required
		// tokenChangedHandler(...args) {
		// 	console.log('tokenChangedHandler', args);
		// },
		// onTokenRefreshError() {
		// 	console.log('onTokenRefreshError');
		// },
		// onVerifyTokenError() {
		// 	console.log('onVerifyTokenError');
		// },
		// Required in most cases.
		firebaseAdminInitConfig: {
			credential: {
				projectId,
				clientEmail,
				privateKey,
			},
			databaseURL: `https://${projectId}.firebaseio.com`,
		},
		firebaseClientInitConfig: {
			apiKey,
			authDomain,
			databaseURL: `https://${projectId}.firebaseio.com`,
			projectId,
		},
		cookies: {
			name: COOKIE_NAME,
			keys: [COOKIE_SECRET],
			httpOnly: true,
			maxAge: 14 * 60 * 60 * 24 * 1000, // fourteen days
			overwrite: true,
			path: '/',
			sameSite: 'strict',
			secure: !IS_DEV,
			signed: true,
		},
	});
}

export
function isServerReq(req: IncomingMessage) {
	return !req.url?.startsWith('/_next');
}
