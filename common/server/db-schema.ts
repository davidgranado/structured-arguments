import type { ObjectId, OptionalId } from 'mongodb';
import type {
	Post,
	PostCommon,
	PostResponse,
	UserActivity,
} from '@common/types';

export
type DbBookmark = OptionalId<{
	userId: string;
	postId: ObjectId;
}>;

export
type DbPost = OptionalId<PostCommon>;

export
type DbPostHistory = OptionalId<{
	ownerId: string;
	date: string;
	postContent: Pick<Post, 'body' | 'title'>;
}>;

export
type DbPostOwner = OptionalId<{
	abandoned?: boolean
	postId: ObjectId;
	userId: string;
}>;

export
type DbPostResponse = OptionalId<PostResponse>;

export
type DbPostResponseHistory = OptionalId<{
	comment: string;
	points: number;
	date: string;
	parentPostId: string;
	userId: string;
}>;

export
type DbUserActivity = OptionalId<UserActivity>;

export
interface DbPointTransaction {
	balance: number;
	date: string;
	points: number;
	userId: string;
}

export
interface DbPostPoints {
	date: string;
	userId: string;
	postId: ObjectId;
	points: number;
	comment?: {
		commentUserId: string;
		parentPostId: string;
	};
}
