
import Joi from 'joi';
import type { NewPost, NewPostParentResponse } from '@common/types';
import {
	MAX_POST_BODY_LENGTH,
	MAX_POST_RESPONSE_LENGTH,
	MAX_POST_TITLE_LENGTH,
	MIN_POST_BODY_LENGTH,
	MIN_POST_POINTS,
	MIN_POST_TITLE_LENGTH,
	MONGO_ID_LENGTH,
} from '@common/constants';

export
const points = Joi.number()
	.integer()
	.min(MIN_POST_POINTS)
	.required();

export
const postBody = Joi.string()
	.min(MIN_POST_BODY_LENGTH)
	.max(MAX_POST_BODY_LENGTH);

export
const postTitle = Joi.string()
	.min(MIN_POST_TITLE_LENGTH)
	.max(MAX_POST_TITLE_LENGTH);

export
const mongoId = Joi.string().length(MONGO_ID_LENGTH);

export
const userId = Joi.string().max(128);

export
const postComment = Joi.string()
	.max(MAX_POST_RESPONSE_LENGTH);

export
const newPostResponse = Joi.object<NewPost>({
	title: postTitle.required(),
	body: postBody.required(),
	parentPostResponses: Joi.array().items(
		Joi.object<NewPostParentResponse>({
			comment: postComment.optional(),
			parentPostId: mongoId.required(),
		})
	),
});
