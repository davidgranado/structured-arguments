import { dbClientPromise } from '@common/server/mongodb';
import { ObjectId } from 'mongodb';
import { nowISOString } from '@common/utils';
import type { PostResponse } from '@common/types';
import {
	DbCollections,
	POST_CREATE_POINT_RATIO,
} from '@common/constants';
import {
	DbPost,
	DbPostResponseHistory,
} from '@common/server/db-schema';

interface ResponseUpdateArgs {
	points: number;
	postId: string;
	parentPostId: string;
	userId: string;
	comment: string;
}

export
async function responseUpdate(props: ResponseUpdateArgs) {
	const {
		points,
		postId,
		parentPostId,
		userId,
		comment,
	} = props;

	const spentPoints = points * POST_CREATE_POINT_RATIO;
	const db = await dbClientPromise;
	const posts = db.collection<DbPost>(DbCollections.Posts);
	const post = await posts.findOne<DbPost>({ _id: new ObjectId(postId) });
	const trimmedComment = comment.trim();

	if(!post?._id) {
		throw `Post ${postId} not found.`;
	}

	const date = nowISOString();

	const dbCalls = [];

	const newResponse: PostResponse = {
		parentPostId,
		userId,
		comment: trimmedComment,
		created: date,
		points: spentPoints,
		lastUpdated: date,
	};

	const existingResponse = post.parentPostResponses.find(r => (
		(r.parentPostId === parentPostId) &&
		(r.userId === userId)
	));

	if(existingResponse) {
		newResponse.created = existingResponse.created;

		const archivedResponse: DbPostResponseHistory = {
			date,
			userId,
			parentPostId,
			points: existingResponse.points,
			comment: existingResponse.comment || '',
		};

		dbCalls.push(
			db
				.collection<DbPostResponseHistory>(DbCollections.PostResponseEditHistory)
				.insertOne(archivedResponse)
		);
	}

	if(trimmedComment) {
		newResponse.comment === trimmedComment;
	}

	if(existingResponse) {
		dbCalls.push(
			posts.updateOne(
				{
					_id: new ObjectId(postId),
					'parentPostResponses.userId': userId,
					'parentPostResponses.parentPostId': parentPostId,
				}, {
					$set: {
						points: post.points + spentPoints,
						'parentPostResponses.$.comment': trimmedComment,
						'parentPostResponses.$.lastUpdated': date,
					},
				}
			),
		);
	} else {
		dbCalls.push(
			posts.updateOne(
				{ _id: new ObjectId(postId) },
				{
					points: post.points + spentPoints,
					$push: { parentPostResponses: newResponse },
				}
			)
		);
		dbCalls.push(
			posts.updateOne(
				{ _id: new ObjectId(parentPostId) },
				{ $addToSet: { childPostIds: postId } }
			),
		);
	}

	await Promise.all(dbCalls);
}
