// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import admin, { credential } from 'firebase-admin';
import { firebasePrivateConfig } from '@/common/firebase-config';
import { dbClientPromise } from '../mongodb';
import { ObjectId } from 'mongodb';
import { ActivityTypes, DbCollections } from '@common/constants';
import { decodeBase64, nowISOString } from '@common/utils';
import { verifyIdToken } from 'next-firebase-auth';
export { responseUpdate } from './response-update';
import type { Post } from '@common/types';
import type {
	DbBookmark,
	DbPointTransaction,
	DbPost,
	DbPostOwner,
	DbUserActivity,
} from '../db-schema';

if (admin?.apps.length === 0) {
	admin.initializeApp({ credential: credential.cert(firebasePrivateConfig) });
}

export
async function getPointBalance(userId: string) {
	const db = await dbClientPromise;
	const postTransactions = db.collection<DbPointTransaction>(DbCollections.PointTransactions);

	const result = await postTransactions.find({ userId })
		.sort({ date: -1 })
		.limit(1)
		.toArray();

	return result?.[0]?.balance || 0;
}

export
async function spendPoints(userId: string, points: number) {
	const prevBalance = await getPointBalance(userId);

	if(prevBalance < points) {
		throw 'Insufficient points';
	}

	const db = await dbClientPromise;
	const postTransactions = db.collection<DbPointTransaction>(DbCollections.PointTransactions);

	const newTransaction = {
		points,
		date: nowISOString(),
		userId,
		balance: prevBalance - points,
	};

	await postTransactions.insertOne(newTransaction);

	return newTransaction;
}

export
async function recordActivity(userId: string | null, type: ActivityTypes, params?: any) {
	console.log('recordActivity');

	if(!userId) {
		return;
	}

	const db = await dbClientPromise;

	const activity: DbUserActivity = {
		date: nowISOString(),
		type,
		userId,
	};

	if(params) {
		activity.params = params;
	}

	db
		.collection<DbUserActivity>(DbCollections.UserActivity)
		.insertOne(activity);

}

export
async function parseJwt(authUserTokensCookie: string) {
	if(!authUserTokensCookie) {
		return null;
	}

	const { idToken = '' } = JSON.parse(decodeBase64(authUserTokensCookie));

	if(!idToken) {
		return null;
	}

	return verifyIdToken(idToken);
}

export
async function fetchItem(id: string, userId?: string) {
	console.log('fetchItem');
	const db = await dbClientPromise;
	const postCollection = db.collection<DbPost>(DbCollections.Posts);

	try {
		const parentPosts: DbPost[] = [];
		const post = await postCollection.findOne<DbPost>({ _id: new ObjectId(id) });

		if(!post) {
			throw `Post "${id}" not found`;
		}

		const relatedIds = post
			.parentPostResponses
			.map(r => r.parentPostId)
			.concat(post.childPostIds)
			.map(pId => new ObjectId(pId));

		const related = relatedIds.length ?
			await postCollection
				.find({ _id: { $in: relatedIds } })
				.sort({ created: -1 })
				.limit(50)
				.toArray() :
			[];

		// TODO Clean this mess
		const parentPostResponse = post.parentPostResponses.reduce(
			//@ts-ignore
			(a, b) => {
				if(!a) {
					return b;
				}

				//@ts-ignore
				new Date(a.created) > new Date(b.created) ? a : b;
			},
			null
		);

		if(parentPostResponse) {
			const parentPost = related.find(r => r._id.toString() === parentPostResponse.parentPostId);

			if(parentPost) {
				parentPosts.unshift(parentPost);

				const parentParentPostResponse = parentPost.parentPostResponses.reduce(
					//@ts-ignore
					(a, b) => {
						if(!a) {
							return b;
						}

						//@ts-ignore
						new Date(a.created) > new Date(b.created) ? a : b;
					},
					null
				);

				if(parentParentPostResponse) {
					const parentParentPost = await postCollection
						.findOne({ _id: new ObjectId(parentParentPostResponse.parentPostId) });

					if(parentParentPost) {
						parentPosts.unshift(parentParentPost);
					}
				}
			}
		}


		const bookmarks = userId ?
			await fetchBookmarksFromPostIds(
				userId,
				[post._id?.toString() || '']
					.concat(
						related
							.	map(p => p._id.toString())
					)
					.concat(
						parentPosts
							.map(p => p._id?.toString() || '')
					),
			) :
			[];

		// END TODO Clean this mess

		const formatPost = formatPostWithBookmarks(bookmarks);

		return post && {
			post: formatPost(post),
			related: related.map(formatPost),
			parentPosts: parentPosts.map(formatPost),
		};
	} catch(e) {
		console.error('Error: fetchItem', e);
		return {
			post: null,
			related: [],
			parentPosts: [],
		};
	}
}

export
async function fetchFeed(userId: string): Promise<Post[]> {
	console.log('fetchFeed');
	const db = await dbClientPromise;

	try {
		const results = await db
			.collection<DbPost>(DbCollections.Posts)
			.find({})
			.sort({ created: -1 })
			.limit(20)
			.toArray();

		const bookmarks = userId ?
			await fetchBookmarksFromPostIds(
				userId,
				results.map(r => r._id.toString())
			) :
			[];

		return results.map(formatPostWithBookmarks(bookmarks));
	} catch(e) {
		console.error('Error: fetchFeed', e);
		return [];
	}

}

// TODO Is there a better way to do this in MongoDB
const DOC_PLACEHOLDER = 'docTemp';

export
async function fetchUserBookmarkedPosts(userId: string): Promise<Post[]> {
	console.log('fetchUserBookmarkedPosts');
	const db = await dbClientPromise;

	const result = await db.collection<DbBookmark>(DbCollections.PostBookmarks)
		.aggregate<Post>([
		{ $match: { userId } },
		{ $sort: { date: -1 } },
		{
			$lookup: {
				from: DbCollections.Posts,
				localField: 'postId',
				foreignField: '_id',
				as: DOC_PLACEHOLDER,
			},
		},
		{ $unwind: { path: `$${DOC_PLACEHOLDER}` } },
		{ $replaceRoot: { newRoot: `$${DOC_PLACEHOLDER}` } },
	])
		.toArray();

	return result.map(p => ({
		...p,
		bookmarked: true,
		_id: p._id?.toString(),
	}));
}

export
async function fetchUserPosts(userId: string): Promise<Post[]> {
	const db = await dbClientPromise;

	const result = await db
		.collection<DbPostOwner>(DbCollections.PostOwners)
		.aggregate<Post>([
		{
			$match: {
				userId,
				abandoned: { $not: { $eq: true } },
			},
		},
		{
			$lookup: {
				from: DbCollections.Posts,
				localField: 'postId',
				foreignField: '_id',
				as: DOC_PLACEHOLDER,
			},
		},
		{ $unwind: { path: `$${DOC_PLACEHOLDER}` } },
		{ $replaceRoot: { newRoot: `$${DOC_PLACEHOLDER}` } },
		{ $sort: { created: -1 } },
		{
			$lookup: {
				from: DbCollections.PostBookmarks,
				localField: '_id',
				foreignField: 'postId',
				as: 'bookmarked',
			},
		},
	]).toArray();

	return result.map(p => ({
		...p,
		bookmarked: !!(p.bookmarked as any).length,
		_id: p._id?.toString(),
	}));
}

async function fetchBookmarksFromPostIds(userId: string, postIds: string[]) {
	const db = await dbClientPromise;
	const bookmarks = await db
		.collection<DbBookmark>(DbCollections.PostBookmarks)
		.find({
			userId,
			postId: { $in: postIds.map(p => new ObjectId(p)) },
		})
		.toArray();

	return bookmarks.map(b => b.postId.toString());

}

function formatPostWithBookmarks(bookmarkIds: string[]) {
	return (post: DbPost): Post => {
		const _id = post._id?.toString();

		return {
			...post,
			_id,
			bookmarked: !!_id && bookmarkIds.includes(_id),
		};
	};
}
