import { Paths } from '@common/constants';
import { Post } from '@common/types';
import { urlJoin } from './utils';

export
function getItemUrl(item: Post) {
	if(!item._id) {
		return '';
	}

	return urlJoin(location.host, Paths.Post, item._id);
}
