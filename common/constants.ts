import { State } from '@common/types';

export
enum Paths {
	Home = '/',
	Post = '/p',
	Bookmarks = '/b',
	UserBoosts = '/u/b',
	UserPosts = '/u/p',
	Settings = '/settings',
}

export
const IS_SSR = typeof window === 'undefined';

export
const API_URL = '/api/v0';

export
const COOKIE_NAME = 'StructuredArguments';

export
const AUTH_USER_COOKIE_NAME = `${COOKIE_NAME}.AuthUser`;

export
const AUTH_USER_TOKENS_COOKIE_NAME = `${COOKIE_NAME}.AuthUserTokens`;

export
const IS_DEV = process.env.NODE_ENV !== 'production';

export
const MIN_POST_BODY_LENGTH = 10;

export
const MAX_POST_BODY_LENGTH = 500;

export
const MAX_POST_RESPONSE_LENGTH = 200;

export
const MIN_POST_TITLE_LENGTH = 6;

export
const MAX_POST_TITLE_LENGTH = 100;

export
const MIN_POST_POINTS = 10;

export
const MONGO_ID_LENGTH = 24;

export
const NOOP = () => {};

export
const POST_CREATE_POINT_RATIO = 0.5;

export
const MONGODB_DB = 'structured-arguments';

export
const NBSP = '\u00A0';

export
const DOT = '\u2022';

export
const BASE_REQ: RequestInit = {
	credentials: 'include',
	headers: {
		Accept: 'application/json, text/plain, */*',
		'Content-Type': 'application/json',
	},
};

export
const DefaultState: State = {
	app: null,
	auth: null,
	activeBoostPost: null,
	feed: [],
	toastMsg: '',
	loading: false,
	user: null,
	pointSpendPrompt: null,
};

export
enum ActivityTypes {
	BookmarkListView = 'bookmark-list-view',
	FeedView = 'feed-view',
	PostView = 'post-view',
}

export
enum DbCollections {
	Posts = 'posts',
	PostBookmarks = 'post-bookmarks',
	PostEditHistory = 'post-edit-history',
	PostOwners = 'post-owners',
	PostResponseEditHistory = 'post-response-edit-history',
	PointTransactions = 'point-transactions',
	PostPoints = 'post-points',
	UserActivity = 'user-activity',
}

export
enum ModalActions {
	ConnectPost = 'connect-post',
	Edit = 'edit',
	Login = 'login',
	Register = 'register',
	Reply = 'reply',
}
