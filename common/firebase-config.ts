const PROJECT_ID = 'structured-arguments';

export
const firebasePublicConfig = {
	apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY || '',
	appId: '1:117354500398:web:2403138a39ce0809f4aabf',
	authDomain: `${PROJECT_ID}.firebaseapp.com`,
	measurementId: 'G-8R9ZRVXZ0P',
	messagingSenderId: '117354500398',
	projectId: `${PROJECT_ID}`,
	storageBucket: `${PROJECT_ID}.appspot.com`,
};

export
const firebasePrivateConfig = {
	...firebasePublicConfig,
	clientEmail: process.env.FIREBASE_CLIENT_EMAIL || '',
	privateKey: process.env.FIREBASE_PRIVATE_KEY || '',
};
