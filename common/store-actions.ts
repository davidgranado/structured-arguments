import { store } from '@common/store';
import { getApp, initializeApp } from 'firebase/app';
import { firebasePublicConfig } from './firebase-config';
import { DefaultState } from './constants';
import {
	createUserWithEmailAndPassword,
	getAuth,
	onAuthStateChanged,
	signInWithEmailAndPassword,
	sendSignInLinkToEmail,
	signOut,
} from 'firebase/auth';
import { getPointBalance } from './service-calls';
import { Post } from './types';

// @ts-ignore
globalThis.sendSignInLinkToEmail = sendSignInLinkToEmail;

export
async function init() {
	const {
		app,
		auth,
	} = store.getState();

	if(app && auth) {
		return;
	}

	const newApp = getApp() || initializeApp(firebasePublicConfig);
	const newAuth = getAuth(newApp);

	store.setState({
		app: newApp,
		auth: newAuth,
	});
	registerAuthHandler();
}

export
let registerAuthHandler = function() {
	const { auth } = store.getState();

	if(!auth) {
		return;
	}

	onAuthStateChanged(
		auth,
		async newUser => {
			console.log('Auth change', newUser);
			const { user } = store.getState();

			if(newUser?.email && !user) {
				store.setState({
					user: {
						email: newUser.email,
						id: newUser.uid,
						pointBalance: 0,
					},
				});
			}

			if(!newUser) {
				store.setState({ user: null });
			}
		},
	);

	// Figure out better pattern to prevent double register
	registerAuthHandler = () => null;
};

export
async function login(email: string, password: string) {
	const { auth } = store.getState();
	let success = false;

	if(!auth) {
		return success;
	}

	store.setState({ loading: true });

	try {
		await signInWithEmailAndPassword(auth, email, password);
		success = true;
	} catch (e: any) {
		setToastMsg(e.message);
	}

	store.setState({ loading: false });
	console.log('login done');
	setToastMsg('Successfully logged in');
	return success;
}

export
async function logout() {
	const {
		auth,
		feed,
	} = store.getState();

	if(!auth) {
		return;
	}

	console.log('logout');
	await signOut(auth);
	store.setState({
		...DefaultState,
		feed,
	});

	setToastMsg('Successfully logged out');
	await init();
	console.log('logout done');
}

export
function setLoading(loading: boolean) {
	store.setState({ loading });
}

export
function setToastMsg(toastMsg: string) {
	store.setState({ toastMsg });
}

export
function setActiveBoostPost(post: Post) {
	store.setState({ activeBoostPost: post });
}

export
function confirmSpend(points: number, isSelfSpend = false): Promise<boolean> {
	return new Promise((resolve) => {
		store.setState({
			pointSpendPrompt: {
				points,
				isSelfSpend,
				onConfirm() {
					store.setState({ pointSpendPrompt: null });
					resolve(true);
				},
				onCancel() {
					store.setState({ pointSpendPrompt: null });
					resolve(false);
				},
			},
		});
	});
}

export
function unsetActiveBoostPost() {
	store.setState({ activeBoostPost: null });
}

export
async function refreshPointBalance() {
	console.log('refreshPointBalance');
	const { user } = store.getState();

	if(!user) {
		return;
	}

	const pointBalance = await getPointBalance();

	store.setState({
		user: {
			...user,
			pointBalance,
		},
	});

	console.log('refreshPointBalance done');
}

export
async function register(email: string, password: string) {
	const { auth } = store.getState();
	let success = false;

	if(!auth) {
		return;
	}

	console.log('register');
	store.setState({ loading: true });

	try {
		await createUserWithEmailAndPassword(auth, email, password);
		success = true;
	} catch (e: any) {
		setToastMsg(e.message);
	}

	store.setState({ loading: false });
	console.log('register done');
	return success;
}
