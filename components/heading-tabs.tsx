import Link from 'next/link';
import { useRouter } from 'next/router';
import {
	Box,
	Tab,
	Tabs,
} from '@mui/material';

export
function HeadingTabs() {
	const { query } = useRouter();
	const { foo = ['hot'] } = query;

	return (
		<Box sx={{
			marginBottom: 1,
			borderBottom: 1,
			borderColor: 'divider',
		}}>
			<Tabs value={foo[0]}>
				{/** @ts-ignore for "value" prop being passed to tab, which can't be set directly for some reason*/}
				<Link value="hot"
					passHref
					href="/"
				>
					<Tab LinkComponent="a" label="Hot" />
				</Link>
				{/** @ts-ignore for "value" prop being passed to tab, which can't be set directly for some reason*/}
				<Link value="new"
					passHref
					href="/new"
				>
					<Tab LinkComponent="a" label="New" value="new" />
				</Link>
				{/** @ts-ignore for "value" prop being passed to tab, which can't be set directly for some reason*/}
				<Link value="top"
					passHref
					href="/top"
				>
					<Tab LinkComponent="a" label="Top" />
				</Link>
			</Tabs>
		</Box>
	);
}
