import { SearchIcon } from './icons';
import {
	InputAdornment,
	TextField,
} from '@mui/material';

export
function SearchField() {
	return (
		<TextField
			fullWidth
			placeholder="Search"
			type="search"
			InputProps={{
				autoComplete: 'off',
				endAdornment: (
					<InputAdornment position="end">
						<SearchIcon />
					</InputAdornment>
				),
			}}
		/>
	);
}
