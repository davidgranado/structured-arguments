import { Post } from '@common/types';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useAuthUser } from 'next-firebase-auth';
import { ModalActions } from '@common/constants';
import { BookmarkToggle } from '@components/bookmark-toggle';
import { formatCompactNumber } from '@common/utils';
import { setActiveBoostPost } from '@common/store-actions';
import {
	BoostIcon,
	CommentIcon,
	ConnectPostIcon,
} from '@components/icons';
import {
	Button,
	Grid,
	Tooltip,
} from '@mui/material';

interface Props {
	post: Post;
	parentId?: string;
	size?: 'small' | 'medium' | 'large';
}

export
function PostActions(props: Props) {
	const {
		post,
		size = 'medium',
	} = props;
	const {
		pathname,
		query,
	} = useRouter();
	const user = useAuthUser();

	return (
		<>
			<Grid
				xs
				item
				style={{ textAlign: 'center' }}
			>
				<Link
					passHref
					shallow
					href={{
						pathname,
						query: {
							...query,
							a: ModalActions.Reply,
							parentPostId: post._id,
						},
					}}
				>
					<Tooltip title="Add Point/Counter Point">
						<Button
							size={size}
							startIcon={<CommentIcon/>}
						>
							{post.childPostIds.length || ''}
						</Button>
					</Tooltip>
				</Link>
			</Grid>
			<Grid
				xs
				item
				style={{ textAlign: 'center' }}
			>
				<Link
					passHref
					shallow
					href={{
						pathname,
						query: {
							...query,
							a: ModalActions.ConnectPost,
							responsePostId: post._id,
						},
					}}
				>
					<Tooltip title="Connect to Post as Response">
						<Button
							size={size}
							startIcon={<ConnectPostIcon/>}
						>
							{post.parentPostResponses.length || ''}
						</Button>
					</Tooltip>
				</Link>
			</Grid>
			<Grid
				xs
				item
				style={{ textAlign: 'center' }}
			>
				<Tooltip title="Boost">
					<Button
						size={size}
						color="success"
						startIcon={<BoostIcon fontSize="inherit" />}
						onClick={() => setActiveBoostPost(post)}
					>
						{formatCompactNumber(post.points)}
					</Button>
				</Tooltip>
			</Grid>
			<Grid
				xs
				item
				style={{ textAlign: 'center' }}
			>
				<BookmarkToggle
					size={size}
					isLoggedIn={!!user}
					post={post}
				/>
			</Grid>
		</>
	);
}
