import { useStore } from '@common/store';
import { Backdrop, CircularProgress } from '@mui/material';

export
function LoadingOverlay() {
	const loading = useStore(s => s.loading);

	return (
		<Backdrop
			open={loading}
			sx={{ zIndex: (theme) => theme.zIndex.snackbar + 1 }}
		>
			<CircularProgress />
		</Backdrop>
	);
}
