import { Paths } from '@common/constants';
import Link from 'next/link';
import { useRouter } from 'next/router';
import {
	Divider,
	Fab,
	Box,
	List,
	ListItem,
	ListItemIcon,
	ListItemText,
} from '@mui/material';
import {
	BookmarkIcon,
	BookmarkOutlinedIcon,
	BoostIcon,
	BoostOutlineIcon,
	CreateIcon,
	EditIcon,
	HomeIcon,
	HomeOutlinedIcon,
	PostIcon,
	PostOutlinedIcon,
} from '@components/icons';

const TopNavItems = [
	{
		label: 'Home',
		icon: <HomeOutlinedIcon/>,
		selectedIcon: <HomeIcon/>,
		path: Paths.Home,
	}, {
		label: 'Bookmarks',
		icon: <BookmarkOutlinedIcon/>,
		selectedIcon: <BookmarkIcon/>,
		path: Paths.Bookmarks,
	}, {
		label: 'My Posts',
		icon: <PostOutlinedIcon/>,
		selectedIcon: <PostIcon/>,
		path: Paths.UserPosts,
	}, {
		label: 'My Boosts',
		icon: <BoostOutlineIcon/>,
		selectedIcon: <BoostIcon/>,
		path: Paths.UserBoosts,
	},
];

// const BottomNavItems = [
// 	{
// 		label: 'Settings',
// 		icon: <SettingsOutlinedIcon/>,
// 		selectedIcon: <SettingsIcon/>,
// 		path: Paths.Settings,
// 	},
// ];

export
function LeftRail() {
	const {
		pathname,
		query,
		asPath,
	} = useRouter();

	return (
		<List>
			{TopNavItems.map(i => (
				<Foo
					{...i}
					key={i.path}
					selected={asPath === i.path}
				/>
			))}
			{/* <Divider/> */}
			{/* {BottomNavItems.map(i => (
				<Foo
					{...i}
					key={i.path}
					selected={asPath === i.path}
				/>
			))} */}
			<Divider/>
			<ListItem style={{ justifyContent: 'center' }}>
				<Box
					width="100%"
					sx={{
						display: {
							xs: 'none',
							md: 'block',
						},
					}}
				>
					<CreatePostButton/>
				</Box>
				<Box
					sx={{
						display: {
							xs: 'inline',
							md: 'none',
						},
					}}
				>
					<Link
						passHref
						shallow
						href={{
							pathname,
							query: {
								a: ModalActions.Edit,
								...query,
							},

						}}
					>
						<Fab color="primary" size="small">
							<EditIcon />
						</Fab>
					</Link>
				</Box>
			</ListItem>
		</List>
	);
}

interface FooProps {
	path: string;
	icon: JSX.Element;
	selectedIcon: JSX.Element;
	label: string;
	selected?: boolean;
}

function Foo(props: FooProps) {
	const {
		path,
		icon,
		selectedIcon,
		label,
		selected,
	} = props;
	return (
		<Link
			passHref
			href={path}
			key={label}
		>
			<ListItem
				component="a"
				style={{ justifyContent: 'center' }}
				selected={selected}
			>
				<ListItemIcon style={{ justifyContent: 'center' }} >
					{selected ? selectedIcon : icon}
				</ListItemIcon>
				<ListItemText
					sx={{
						display: {
							xs: 'none',
							md: 'block',
						},
					}}
					primary={label}
					primaryTypographyProps={selected ? { fontWeight: 'bold' } : undefined}
				/>
			</ListItem>
		</Link>
	);
}

import { Button } from '@mui/material';
// import { useRouter } from 'next/router';
// import Link from 'next/link';
import { ModalActions } from '@common/constants';

function CreatePostButton() {
	const {
		pathname,
		query,
	} = useRouter();

	return (
		<Link
			passHref
			shallow
			href={{
				pathname,
				query: {
					a: ModalActions.Edit,
					...query,
				},

			}}
		>
			<Button
				fullWidth
				disableElevation
				variant="contained"
				endIcon={<CreateIcon/>}
			>
				Create Post
			</Button>
		</Link>
	);
}
