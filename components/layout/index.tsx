import Head from 'next/head';
import { TitleBar } from '@components/title-bar';
import { RightRail } from '@components/right-rail';
import { PostItemModals } from '@components/modals/post-item-modals';
import { LeftRail } from '@components/layout/left-rail';
import type { FC } from 'react';
import {
	Container,
	Grid,
} from '@mui/material';

interface Props {
	title?: string;
}

export
const Layout: FC<Props> = (props) => {
	const {
		title, children,
	} = props;

	return (
		<>
			<Head>
				<title>Structured Arguments {title ? `- ${title}` : ''}</title>
				<meta name="description" content="Strctured Arguments - Where Ideas Reign King" />
				<link rel="icon" href="/favicon.ico" />
			</Head>

			<TitleBar/>

			<Container>
				<Grid container columns={16} spacing={2}>
					<Grid
						item
						xs={2}
						md={4}
					>
						<LeftRail/>
					</Grid>
					<Grid
						item
						xs={14}
						md={8}
					>
						{children}
					</Grid>
					<Grid
						item
						// sm={4}
						md={4}
						sx={{
							display: {
								xs: 'none',
								sm: 'block',
							},
						}}
					>
						<RightRail/>
					</Grid>
				</Grid>
			</Container>
			<PostItemModals/>
		</>
	);
};
