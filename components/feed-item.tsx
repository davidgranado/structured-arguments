import { Post } from '@common/types';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useAuthUser } from 'next-firebase-auth';
import { ModalActions, Paths } from '@common/constants';
import { DropdownMenu } from '@components/dropdown-menu';
import { getItemUrl } from '@common/fns';
import { PostActions } from './post-actions';
import {
	getTimeSinceDate,
	localizedDateFormat,
	parseContentString,
	urlJoin,
	writeToClipboard,
} from '@common/utils';
import {
	Box,
	Grid,
	Typography,
	Link as MuiLink,
	MenuItem,
	Tooltip,
} from '@mui/material';

interface Props {
	post: Post;
	parentId?: string;
}

export
function FeedItem(props: Props) {
	const {
		post,
		parentId = '',
	} = props;
	const {
		pathname,
		query,
	} = useRouter();
	const user = useAuthUser();

	const parentReply = post.parentPostResponses.find(r => r.parentPostId === parentId);
	const isOwner = post.ownerId === user.id;

	return (
		<Box sx={{
			borderBottom: '1px solid',
			borderColor: 'text.disabled',
			padding: 1,
		}}>
			<div>
				{!!parentReply?.comment && (
					<Typography variant="subtitle1">
						<em>
							Comment: {parentReply.comment}
						</em>
					</Typography>
				)}
				<div
					style={{
						cursor: 'pointer',
						fontWeight: 'bold',
					}}
				>
					<Grid
						container
						spacing={2}
						wrap="nowrap"
						justifyContent="center"
						alignItems="center"
					>
						<Grid
							item
							xs
							zeroMinWidth
						>
							<Link href={urlJoin(Paths.Post, post._id)} passHref>
								<Typography
									noWrap
									component={MuiLink}
									variant="body1"
									title={post.title}
									style={{
										fontWeight: 'bold',
										display: 'block',
									}}
								>
									{post.title}
								</Typography>
							</Link>
						</Grid>
						<Grid
							item
							xs={5}
							sm={4}
							lg={3}
						>
							<Typography
								color="text.secondary"
								sx={{
									fontSize: 14,
									textAlign: 'right',
								}}
							>
								<Link href={urlJoin(Paths.Post, post._id)} passHref>
									<Tooltip title={localizedDateFormat(post.created)} suppressHydrationWarning>
										<strong>
											{getTimeSinceDate(post.created)}
										</strong>
									</Tooltip>
								</Link>
							</Typography>
						</Grid>
						<Grid
							item
							xs={1}
						>
							<DropdownMenu>
								{isOwner && (
									[
										<Link
											key="a"
											passHref
											shallow
											href={{
												pathname,
												query: {
													...query,
													a: ModalActions.Edit,
													postId: post._id,
												},
											}}
										>
											<MenuItem>Edit</MenuItem>
										</Link>,
										<MenuItem key="b">Abandon Ownership</MenuItem>,
									]
								)}
								<MenuItem onClick={() => writeToClipboard(getItemUrl(post))}>
									Copy Link
								</MenuItem>
								<MenuItem>View Point Activity</MenuItem>
								{!isOwner && (
									<MenuItem>Mark as Spam</MenuItem>
								)}
							</DropdownMenu>
						</Grid>
					</Grid>
				</div>
				<Typography
					variant="body1"
					sx={{ mb: 1.5 }}
					style={{ overflowWrap: 'break-word' }}
					dangerouslySetInnerHTML={{ __html: parseContentString(post.body) }}
				/>
				<Grid container columns={4} alignItems="flex-end">
					<PostActions
						size="medium"
						post={post}
					/>
				</Grid>
			</div>
		</Box>
	);
}
