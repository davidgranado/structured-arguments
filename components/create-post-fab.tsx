import { useRouter } from 'next/router';
import { Box, Fab } from '@mui/material';
import Link from 'next/link';
import { ModalActions } from '@common/constants';
import { CreateIcon } from './icons';

export
function CreatePostFab() {
	const {
		pathname,
		query,
	} = useRouter();

	return (
		<Box
			sx={{
				display: {
					xs: 'block',
					sm: 'none',
				},
			}}
		>
			<Link
				passHref
				shallow
				href={{
					pathname,
					query: {
						a: ModalActions.Edit,
						...query,
					},

				}}
			>
				<Fab
					color="primary"
					sx={{
						position: 'fixed',
						bottom: 16,
						right: 16,
					}}
				>
					<CreateIcon />
				</Fab>
			</Link>
		</Box>
	);
}
