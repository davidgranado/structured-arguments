import { TextField } from '@mui/material';
import { ChangeEvent, useState } from 'react';

interface Props {
	autoFocus?: boolean;
	disabled?: boolean;
	fullWidth?: boolean;
	helperText: string;
	label: string;
	margin?: 'dense' | 'normal' | 'none';
	minLength: number;
	minRows?: number;
	multiline?: boolean;
	value: string;
	variant?: 'filled' | 'outlined' | 'standard';
	onChange?: (e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => void;
}

export
function DelayedErrorTextInput(props: Props) {
	const {
		autoFocus,
		disabled,
		fullWidth,
		helperText,
		label,
		margin,
		minLength,
		minRows,
		multiline,
		variant,
		value,
		onChange = () => null,
	} = props;
	const [touched, setTouched] = useState(false);

	const error = touched && value.length < minLength;

	function handleTouched() {
		!touched && setTouched(true);
	}

	return (
		<TextField
			margin={margin}
			error={error}
			autoFocus={autoFocus}
			disabled={disabled}
			fullWidth={fullWidth}
			helperText={error && helperText}
			label={label}
			minRows={minRows}
			multiline={multiline}
			value={value}
			variant={variant}
			onChange={onChange}
			onBlur={handleTouched}
		/>
	);
}
