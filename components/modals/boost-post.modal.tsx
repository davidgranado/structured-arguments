import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useStore } from '@common/store';
import { POST_CREATE_POINT_RATIO } from '@common/constants';
import { ClampedNumberInput } from '@components/clamped-number-input';
import { useState } from 'react';
import { Alert } from '@mui/material';
import { boostPost } from '@common/service-calls';
import { useSoftReload } from '@common/hooks';
import { BoostIcon } from '@components/icons';
import {
	confirmSpend,
	refreshPointBalance,
	setLoading,
	unsetActiveBoostPost,
} from '@common/store-actions';

export
function BoostPostModal() {
	const user = useStore(s => s.user);
	const post = useStore(s => s.activeBoostPost);
	const reload = useSoftReload();
	const [points, setPoints] = useState(1);

	if(!(user && post)) {
		return null;
	}

	const isOwner = user.id === post.ownerId;
	const pointSpend = isOwner ? points / POST_CREATE_POINT_RATIO : points;
	const maxSpend = isOwner ? user.pointBalance * POST_CREATE_POINT_RATIO : points;

	const noBalance = !user.pointBalance;

	async function handleBoostPost() {
		if(!post?._id) {
			return;
		}

		if(!await confirmSpend(pointSpend, post.ownerId === user?.id)) {
			return;
		}

		setLoading(true);
		await boostPost(post._id, pointSpend);
		await refreshPointBalance();
		await reload();
		setLoading(false);
		unsetActiveBoostPost();
	}

	return (
		<Dialog open={!!post} onClose={unsetActiveBoostPost}>
			{noBalance && (
				<Alert severity="error">
					No points available.
				</Alert>
			)}
			<DialogTitle>Boost</DialogTitle>
			<DialogContent>
				<DialogContentText>
					<strong>
						{user.pointBalance.toLocaleString()}pts
					</strong><br/>
					- {pointSpend.toLocaleString()}
					<hr/>
				</DialogContentText>
				<DialogContentText>
					<strong>
						{(user.pointBalance - pointSpend).toLocaleString()}
					</strong> remaining
				</DialogContentText>
				<ClampedNumberInput
					autoFocus
					label="Boost Amount"
					disabled={noBalance}
					min={0}
					max={maxSpend}
					value={points}
					onChange={setPoints}
				/>
			</DialogContent>
			<DialogActions>
				<Button
					color="error"
					onClick={unsetActiveBoostPost}
				>
						Cancel
				</Button>
				<Button
					disabled={noBalance || !points}
					color="success"
					endIcon={<BoostIcon/>}
					onClick={handleBoostPost}
				>
						Boost
				</Button>
			</DialogActions>
		</Dialog>
	);
}
