import { BoostPostModal } from './boost-post.modal';
import { ConfirmSpendModal } from './confirm-spend.modal';
import { ConnectPostModal } from './connect-post.modal';
import { EditPostModal } from './edit-post.modal';

export
function PostItemModals() {
	return (
		<>
			<BoostPostModal/>
			<ConnectPostModal/>
			<EditPostModal/>
			<ConfirmSpendModal/>
		</>
	);
}
