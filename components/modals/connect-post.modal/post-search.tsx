import { getPost } from '@common/service-calls';
import { setToastMsg } from '@common/store-actions';
import { enterKeyHandler } from '@common/utils';
import { SearchIcon } from '@components/icons';
import { Post } from '@common/types';
import {
	FormControl,
	IconButton,
	InputAdornment,
	InputLabel,
	OutlinedInput,
} from '@mui/material';

interface Props {
	childPostId: string;
	searchString: string;
	onSearchStringChange(newString: string): void;
	onParentPostLoad(post: Post): void;
}

export
function PostSearch(props: Props) {
	const {
		childPostId,
		searchString,
		onSearchStringChange,
		onParentPostLoad: onLoadParentPost,
	} = props;


	async function loadParentPost() {
		let parentId = searchString;

		try {
			const url = new URL(searchString);
			parentId = url.pathname.split('/')[2] || searchString;
		} catch(e) {
			console.log(`Not a URL: ${searchString}`);
		}

		if(parentId === childPostId) {
			setToastMsg('Posts cannot respond to themselves');
			return;
		}

		try {
			const p = await getPost(parentId);

			if(!p) {
				setToastMsg(`Can't find post "${parentId}"`);
				return;
			}

			onLoadParentPost(p);
		} catch {
			setToastMsg(`There was a problem finding post "${parentId}"`);
		}
	}

	return (
		<FormControl fullWidth>
			<InputLabel>
				Post URL or ID
			</InputLabel>
			<OutlinedInput
				autoFocus
				label="Post URL or ID"
				onChange={e => onSearchStringChange(e.target.value)}
				onKeyUp={e => enterKeyHandler(e.key, loadParentPost)}
				value={searchString}
				endAdornment={
					<InputAdornment position="end">
						<IconButton onClick={loadParentPost}>
							<SearchIcon fontSize="small"/>
						</IconButton>
					</InputAdornment>
				}
			/>
		</FormControl>
	);
}
