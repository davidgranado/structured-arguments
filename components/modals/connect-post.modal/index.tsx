import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import {
	boostPost, connectPost, getPost,
} from '@common/service-calls';
import { Post, PostResponse } from '@common/types';
import { confirmSpend, setToastMsg } from '@common/store-actions';
import {
	exec, formatDate, multiplyList,
} from '@common/utils';
import SwapVertIcon from '@mui/icons-material/SwapVert';
import { useStore } from '@common/store';
import { ClampedNumberInput } from '@components/clamped-number-input';
import { PostSearch } from './post-search';
import { LoginPromptContent } from './login-prompt-content';
import {
	MIN_POST_POINTS,
	ModalActions,
	NBSP,
} from '@common/constants';
import {
	Alert,
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	Grid,
	List,
	ListItemIcon,
	ListItemText,
	MenuItem,
	Tab,
	Tabs,
	TextField,
	useMediaQuery,
	useTheme,
} from '@mui/material';
import { BoostIcon, CommentIcon } from '@components/icons';

interface Query {
	a?: string;
	responsePostId?: string;
	parentPostId?: string;
}

export
function ConnectPostModal() {
	const user = useStore(s => s.user);
	const router = useRouter();
	const theme = useTheme();
	const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
	const [responseComment, setResponseComment] = useState('');
	const [parentPost, setParentPost] = useState<Post | null>(null);
	const [childPost, setChildPost] = useState<Post | null>(null);
	const [parentSearchString, setParentSearchString] = useState('');
	const [points, setPoints] = useState(MIN_POST_POINTS);
	const isLoggedIn = !!user;
	const pointBalance = user?.pointBalance || 0;
	const insufficientBalance = MIN_POST_POINTS > pointBalance;
	const existingResponses = childPost
		?.parentPostResponses
		.filter(r =>
			r.parentPostId === parentPost?._id
		) || [];
	const existingUserResponse = getExistingUserResponse(existingResponses, parentPost, user?.id,);

	const {
		pathname,
		query,
	} = router;

	const {
		a: action,
		responsePostId: childPostId = '',
		...newQuery
	}: Query = query;

	const isOpen = action === ModalActions.ConnectPost;
	const isValid = !!(
		childPostId &&
		parentPost?._id &&
		!insufficientBalance
	);

	useEffect(() => {
		if(childPostId) {
			exec(async () => {
				setChildPost(await getPost(childPostId));
			});
		} else {
			setChildPost(null);
		}
	}, [childPostId]);

	useEffect(() => {
		setResponseComment(existingUserResponse?.comment || '');
	}, [existingUserResponse]);

	function close() {
		setResponseComment('');
		setParentSearchString('');
		setParentPost(null);
		router.back();
	}

	async function save() {
		if(!parentPost?._id) {
			return;
		}

		try {
			if(await confirmSpend(points, true)) {
				await connectPost(childPostId, parentPost._id, points, responseComment);
				close();
			}
		} catch(e: any) {
			console.error(e);
			setToastMsg(`There was a problem: ${e.message}`);
		}
	}

	async function handleCommentBoost(userId: string) {
		if(await confirmSpend(points, userId === user?.id)) {
			await boostPost(childPostId, points, userId, parentPost?._id);
			close();
		}
	}

	return (
		<Dialog
			fullWidth
			fullScreen={fullScreen}
			open={isOpen}
		>
			{isLoggedIn && (
				<>
					{insufficientBalance && (
						<Alert severity="error">
							Insufficient point balanace to create a post connection.{NBSP}
							<strong>{MIN_POST_POINTS}pts</strong> required.{NBSP}
							<strong>{pointBalance}pts</strong> vailable.
						</Alert>
					)}
					<DialogTitle>
						Connect Post as Response
					</DialogTitle>
					<DialogContent>
						<Box style={{ paddingTop: 10 }}>
							<PostSearch
								childPostId={childPostId}
								searchString={parentSearchString}
								onSearchStringChange={setParentSearchString}
								onParentPostLoad={setParentPost}
							/>
						</Box>
					</DialogContent>
					{parentPost && (
						<DialogContent>
							<DialogContentText variant="h5">
								{parentPost.title}
							</DialogContentText>
							<DialogContentText>
								<span dangerouslySetInnerHTML={{ __html: parentPost.body }}/>
							</DialogContentText>
							<DialogContentText>
									Points: {parentPost.points.toLocaleString()}
							</DialogContentText>
						</DialogContent>
					)}
					{childPost && (
						<>
							{parentPost?._id && (
								<DialogContent>
									<ClampedNumberInput
										fullWidth
										label="Comment & Post Boost Points"
										disabled={insufficientBalance}
										value={points}
										min={MIN_POST_POINTS}
										max={pointBalance || MIN_POST_POINTS}
										onChange={newVal => setPoints(newVal)}
									/>
									<Foo
										responses={existingResponses}
										hasExistingResponse={!!existingUserResponse}
										editableComment={responseComment}
										onSetComment={setResponseComment}
										onCommentBoost={handleCommentBoost}
									/>
								</DialogContent>
							)}
							<DialogContent>
								<DialogContentText variant="h5">
									{childPost.title}
								</DialogContentText>
								<DialogContentText>
									<span dangerouslySetInnerHTML={{ __html: childPost.body }}/>
								</DialogContentText>
							</DialogContent>
						</>
					)}
					<DialogActions>
						<Button color="error" onClick={close}>
							Cancel
						</Button>
						<Button
							color="primary"
							disabled={!isValid}
							onClick={save}
						>
							{
								existingUserResponse ?
									'Update response comment' :
									'Connect as Response'
							}
						</Button>
					</DialogActions>
				</>
			)}
			{!isLoggedIn && (
				<LoginPromptContent
					backQuery={newQuery}
					pathname={pathname}
				/>
			)}
		</Dialog>
	);
}

interface FooProps {
	responses: PostResponse[];
	hasExistingResponse?: boolean;
	editableComment: string;
	onSetComment(newComment: string): void;
	onCommentBoost(userId: string): void;
}

function Foo(props: FooProps) {
	const{
		responses,
		editableComment,
		hasExistingResponse,
		onSetComment,
		onCommentBoost,
	} = props;
	const [selectedTab, setSelectedTab] = useState(hasExistingResponse ? 1 : 0);

	return (
		<>
			<Tabs
				variant="fullWidth"
				value={selectedTab}
				onChange={(e, newVal) => setSelectedTab(newVal)}
			>
				<Tab
					label="Boost Comment"
					iconPosition="start"
					value={0}
					icon={<BoostIcon/>}
				/>
				<Tab
					iconPosition="start"
					icon={<CommentIcon/>}
					value={1}
					label={`${hasExistingResponse ? 'Update My' : 'Add'} Comment`}
				/>
			</Tabs>
			{selectedTab === 0 && (
				<List style={{
					maxHeight: '250px',
					overflow: 'auto',
				}}>
					{multiplyList(responses, 10).map((r, i) => (
						<MenuItem
							key={`${r.userId}+${r.parentPostId}+${i}`}
							onClick={() => r.userId && onCommentBoost(r.userId)}
						>
							<ListItemIcon>
								<BoostIcon/>
							</ListItemIcon>
							<ListItemText
								secondary={formatDate(r.created)}
							>
								<strong>
									{r.points} pts
								</strong> - {r.comment}
							</ListItemText>
						</MenuItem>
					))}
				</List>
			)}
			{selectedTab === 1 && (
				<Grid
					container
					alignItems="center"
				>
					<Grid
						item
						xs={1}
						columns={12}
					>
						<SwapVertIcon fontSize="large" color="success" />
					</Grid>
					<Grid item xs>
						<TextField
							multiline
							fullWidth
							margin="dense"
							label="Response Comment (optional)"
							variant="outlined"
							minRows={3}
							value={editableComment}
							onChange={e => onSetComment(e.target.value)}
						/>
					</Grid>
				</Grid>
			)}
		</>
	);
}

function getExistingUserResponse(responses: PostResponse[], parentPost?: Post | null, userId?: string) {
	return (
		parentPost?._id &&
		responses
			.find(r => (
				r.userId === userId &&
				r.parentPostId === parentPost?._id
			))
	) || null;
}
