import Link from 'next/link';
import { ModalActions } from '@common/constants';
import {
	Button,
	DialogActions,
	DialogTitle,
} from '@mui/material';

interface Props {
	pathname: string
	backQuery: any;
}

export
function LoginPromptContent(props: Props) {
	const {
		backQuery: query,
		pathname,
	} = props;

	return (
		<>
			<DialogTitle>
				Login to reply
			</DialogTitle>
			<DialogActions>
				<Link
					replace
					passHref
					shallow
					href={{
						pathname,
						query,
					}}
				>
					<Button color="error">
						Close
					</Button>
				</Link>
				<Link
					passHref
					shallow
					href={{
						pathname,
						query: {
							a: ModalActions.Login,
							...query,
						},
					}}
				>
					<Button>
						Login
					</Button>
				</Link>

			</DialogActions>
		</>
	);
}
