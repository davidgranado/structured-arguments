import { NOOP, POST_CREATE_POINT_RATIO } from '@common/constants';
import { useStore } from '@common/store';
import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
} from '@mui/material';

export
function ConfirmSpendModal() {
	const pointSpendPrompt = useStore(s => s.pointSpendPrompt);
	const {
		points = 0,
		isSelfSpend = true,
		onCancel = NOOP,
		onConfirm = NOOP,
	} = pointSpendPrompt || {};

	return (
		<Dialog open={!!pointSpendPrompt}>
			<DialogTitle>
				Spend {points.toLocaleString()} points
			</DialogTitle>
			<DialogContent>
				<DialogContentText>
					Spend {points.toLocaleString()} points on this post?<br/>
					{isSelfSpend && `Since this is your post, ${(points * POST_CREATE_POINT_RATIO).toLocaleString()} points will be applied.`}
				</DialogContentText>
			</DialogContent>
			<DialogActions>
				<Button color="error" onClick={onCancel}>
					Cancel
				</Button>
				<Button color="primary" onClick={onConfirm}>
					Ok
				</Button>
			</DialogActions>
		</Dialog>
	);
}
