import { useRouter } from 'next/router';
import Link from 'next/link';
import {
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle,
	TextField,
	useMediaQuery,
	useTheme,
} from '@mui/material';
import { useEffect, useState } from 'react';
import { Key } from 'ts-key-enum';
import { useAuthUser } from 'next-firebase-auth';
import { login } from '@common/store-actions';
import { ModalActions } from '@common/constants';

export
function LoginModal() {
	const router = useRouter();
	const theme = useTheme();
	const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const { id: userId } = useAuthUser();
	const {
		a: action,
		...newQuery
	} = router.query;
	const isOpen = action === ModalActions.Login;
	const valid = !!(password && email);

	useEffect(() => {
		if(!userId || !isOpen) {
			return;
		}

		router.replace({
			pathname: router.pathname,
			query: newQuery,
		}, undefined, { shallow: true });
	}, [userId]);

	function handleKeyUp(key: string) {
		if(key === Key.Enter) {
			handleLogin();
		}
	}

	async function handleLogin() {
		if(!valid) {
			return;
		}

		if(await login(email, password)) {
			setEmail('');
		}

		setPassword('');
	}

	return (
		<Dialog
			fullScreen={fullScreen}
			open={isOpen}
		>
			<DialogTitle>
				Login
			</DialogTitle>
			<DialogContent>
				<Box
					noValidate
					autoComplete="off"
					component="form"
				>
					<TextField
						autoFocus
						fullWidth
						label="email"
						variant="standard"
						placeholder="email"
						type="email"
						value={email}
						onKeyUp={e => handleKeyUp(e.key)}
						onChange={e => setEmail(e.target.value)}
					/>
					<TextField
						fullWidth
						label="Password"
						variant="standard"
						type="password"
						value={password}
						onKeyUp={e => handleKeyUp(e.key)}
						onChange={e => setPassword(e.target.value)}
					/>
				</Box>
			</DialogContent>
			<DialogActions>
				<Link
					replace
					passHref
					shallow
					href={{
						pathname: router.pathname,
						query: newQuery,
					}}
				>
					<Button color="error">
						Close
					</Button>
				</Link>
				<Button disabled={!valid} onClick={handleLogin}>
					Login
				</Button>
			</DialogActions>
		</Dialog>
	);
}
