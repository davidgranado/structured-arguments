import { useRouter } from 'next/router';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import { refreshPointBalance, setToastMsg } from '@common/store-actions';
import { NewPostParentResponse, Post } from '@common/types';
import { useStore } from '@common/store';
import { exec } from '@common/utils';
import { ClampedNumberInput } from '@components/clamped-number-input';
import { DelayedErrorTextInput } from '@components/delayed-error-text-input';
import {
	createPost,
	editPost,
	getPost,
} from '@common/service-calls';
import {
	MAX_POST_BODY_LENGTH,
	MAX_POST_TITLE_LENGTH,
	MIN_POST_BODY_LENGTH,
	MIN_POST_POINTS,
	MIN_POST_TITLE_LENGTH,
	ModalActions,
	NBSP,
} from '@common/constants';
import {
	Alert,
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	DialogTitle,
	Grid,
	TextField,
	useMediaQuery,
	useTheme,
} from '@mui/material';

interface Query {
	id?: string;
	a?: string;
	postId?: string;
	parentPostId?: string;
}

export
function EditPostModal() {
	const feed = useStore(s => s.feed);
	const user = useStore(s => s.user);
	const [busy, setBusy] = useState(false);
	const [responseComment, setResponseComment] = useState('');
	const [title, setTitle] = useState('');
	const [points, setPoints] = useState(MIN_POST_POINTS);
	const [body, setBody] = useState('');
	const [parentPost, setParentPost] = useState<Post | null>(null);
	const router = useRouter();
	const theme = useTheme();
	const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
	const isLoggedIn = !!user;
	const pointBalance = user?.pointBalance || 0;
	const insufficientBalance = MIN_POST_POINTS > pointBalance;

	const {
		pathname,
		query,
	} = router;

	const {
		a: action,
		postId,
		parentPostId,
		...newQuery
	}: Query = query;

	if(!router.route.includes('[id]')) {
		delete newQuery.id;
	}

	const isOpen = action === ModalActions.Edit || action === ModalActions.Reply;
	const isValid = !insufficientBalance &&
		isOpen &&
		(points >= MIN_POST_POINTS) &&
		(title.length >= MIN_POST_TITLE_LENGTH) &&
		(title.length <= MAX_POST_TITLE_LENGTH) &&
		(body.length >= MIN_POST_BODY_LENGTH) &&
		(body.length <= MAX_POST_BODY_LENGTH);

	useEffect(() => {
		if(!parentPostId) {
			setParentPost(null);
		}

		if(!isOpen) {
			return;
		}

		const p = feed.find(i => i._id === parentPostId) || null;

		p && setParentPost(p);
	}, [parentPostId]);

	useEffect(() => {
		if(!isOpen) {
			return;
		}

		if(!postId) {
			return;
		}

		exec(async () => {
			const p: Post | null = feed.find(i => i._id === postId) || await getPost(postId) || null;

			if(!p) {
				return;
			}

			setTitle(p.title);
			setBody(p.body);
		});

	}, [postId]);

	async function submit() {
		setBusy(true);

		try {
			const parentPosts: NewPostParentResponse[] = [];

			if(parentPostId) {
				parentPosts.push({
					parentPostId,
					comment: responseComment,
				});
			}

			postId ?
				await editPost(postId, {
					title,
					body,
				}) :
				await createPost({
					title,
					body,
					parentPostResponses: parentPosts,
				}, points);

			close();

		} catch {
			setToastMsg('Something went wrong.  Try again');
		}
		setBusy(false);
	}

	function close() {
		setBody('');
		setPoints(MIN_POST_POINTS);
		setTitle('');
		setParentPost(null);
		router.back();

		if(!postId) {
			refreshPointBalance();
		}

	}

	return (
		<Dialog
			fullWidth
			fullScreen={fullScreen}
			open={isOpen}
		>
			{insufficientBalance && (
				<Alert severity="error">
					Insufficient point balanace to create a post.{NBSP}
					<strong>{MIN_POST_POINTS}pts</strong> required.{NBSP}
					<strong>{pointBalance}pts</strong> vailable.
				</Alert>
			)}
			{isLoggedIn && (
				<>
					<DialogTitle>
						{parentPost ? 'Reply' : (
							postId ? 'Edit Post' : 'New Post'
						)}
					</DialogTitle>
					<DialogContent>
						{parentPost && (
							<>
								<DialogContentText variant="h5">
									{parentPost.title}
								</DialogContentText>
								<DialogContentText>
									<span dangerouslySetInnerHTML={{ __html: parentPost.body }}/>
								</DialogContentText>
								<TextField
									multiline
									fullWidth
									margin="dense"
									label="Response Comment (optional)"
									variant="outlined"
									disabled={busy}
									minRows={3}
									value={responseComment}
									onChange={e => setResponseComment(e.target.value)}
								/>
							</>
						)}
						<Box
							noValidate
							autoComplete="off"
							component="form"
						>
							<Grid container>
								<Grid item xs>
									<DelayedErrorTextInput
										autoFocus
										fullWidth
										label="Title"
										variant="standard"
										helperText={`Posts title must be at least ${MIN_POST_TITLE_LENGTH} characters long.`}
										minLength={MIN_POST_TITLE_LENGTH}
										disabled={busy}
										value={title}
										onChange={e => setTitle(e.target.value)}
									/>
								</Grid>
								{!postId && (
									<Grid item xs={2}>
										<ClampedNumberInput
											fullWidth
											label="Points"
											disabled={insufficientBalance || busy}
											value={points}
											min={MIN_POST_POINTS}
											max={pointBalance || MIN_POST_POINTS}
											onChange={newVal => setPoints(newVal)}
										/>
									</Grid>
								)}
							</Grid>
							<DelayedErrorTextInput
								multiline
								fullWidth
								label="Body"
								variant="outlined"
								margin="dense"
								helperText={`Posts body must be at least ${MIN_POST_BODY_LENGTH} characters long.`}
								minLength={MIN_POST_BODY_LENGTH}
								disabled={busy}
								minRows={6}
								value={body}
								onChange={e => setBody(e.target.value)}
							/>
						</Box>
					</DialogContent>
					<DialogActions>
						<Button disabled={busy} color="error" onClick={close}>
							Cancel
						</Button>
						<Button
							color="primary"
							disabled={busy || !isValid}
							onClick={submit}
						>
							{postId ? 'Update' : 'Post'}
						</Button>
					</DialogActions>
				</>
			)}
			{!isLoggedIn && (
				<>
					<DialogTitle>
						Login to reply
					</DialogTitle>
					<DialogActions>
						<Link
							replace
							passHref
							shallow
							href={{
								pathname,
								query: newQuery as any,
							}}
						>
							<Button color="error">
								Close
							</Button>
						</Link>
						<Link
							passHref
							shallow
							href={{
								pathname,
								query: {
									a: ModalActions.Login,
									...newQuery,
								},
							}}
						>
							<Button>
								Login
							</Button>
						</Link>
					</DialogActions>
				</>
			)}
		</Dialog>
	);
}
