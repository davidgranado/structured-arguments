import { DOT } from '@common/constants';
import { Post } from '@common/types';
import { formatCompactNumber, formatDate } from '@common/utils';
import { BoostIcon } from '@components/icons';
import {
	Dialog,
	DialogContent,
	DialogTitle,
	IconButton,
	List,
	ListItem,
	ListItemText,
} from '@mui/material';

interface Props {
	open: boolean;
	post: Post;
	onClose(): void;
}

export
function PostResponseModal(props: Props) {
	const {
		post,
		open,
		onClose,
	} = props;

	return (
		<Dialog
			fullWidth
			open={open}
			onClose={onClose}
		>
			<DialogTitle>
				{post.title}
			</DialogTitle>
			<DialogContent>
				<List>
					{([1, 2, 3, 4, 5, 6]).flatMap(() => post.parentPostResponses.map(r => (
						<ListItem
							key={r.created}
							secondaryAction={
								<IconButton edge="end">
									<BoostIcon />
								</IconButton>
							}
						>
							<ListItemText
								primary={r.comment}
								secondary={`${formatCompactNumber(r.points)} pts ${DOT} ${formatDate(r.created)}`}
							/>
						</ListItem>
					)))}
				</List>
			</DialogContent>
		</Dialog>
	);
}
