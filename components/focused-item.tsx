import { Post } from '@common/types';
import Link from 'next/link';
import { PostActions } from './post-actions';
import { Paths } from '@common/constants';
import { useEffect, useState } from 'react';
import { connectPost, editPost } from '@common/service-calls';
import { useAuthUser } from 'next-firebase-auth';
import { useSoftReload } from '@common/hooks';
import { PostResponseModal } from './modals/post-responses.modal';
import {
	Button,
	ButtonGroup,
	Link as MuiLink,
	TextField,
	Tooltip,
} from '@mui/material';
import {
	parseContentString,
	urlJoin,
	localizedDateFormat,
} from '@common/utils';
import {
	Box,
	Grid,
	IconButton,
	Typography,
} from '@mui/material';
import {
	CancelIcon,
	ConfirmIcon,
	EditIcon,
} from './icons';

interface Props {
	post: Post;
	parentId?: string;
}

export
function FocusedItem(props: Props) {
	const {
		post,
		parentId,
	} = props;
	const reload = useSoftReload();
	const { id: userId } = useAuthUser();
	const [responseModalIsOpen, setResponseModalIsOpen] = useState(false);
	const [isEditing, setIsEditing] = useState(false);
	const [busy, setBusy] = useState(false);
	const [title, setTitle] = useState(post.title);
	const [body, setBody] = useState(post.body);
	const parentPost = post
		.parentPostResponses
		.sort((a, b) => a.points - b.points)
		.find(r => r.parentPostId === parentId);
	const [parentReplyComment, setParentReplyComment] = useState(parentPost?.comment || '');
	const isOwner = post.ownerId === userId;
	const hasParent = !!parentId;
	const hasParentReply = !!parentPost?.comment;
	const isValid = isEditing && (title.length > 3) && (body.length > 3);

	useEffect(() => {
		if(isEditing) {
			return;
		}

		setTitle(post.title);
		setBody(post.body);
	}, [isEditing, post]);

	useEffect(() => {
		if(isEditing) {
			return;
		}

		if(parentPost) {
			setParentReplyComment(parentPost?.comment || '');
		}
	}, [isEditing, parentPost?.comment]);

	async function save() {
		if(!post._id) {
			return;
		}

		setBusy(true);
		const postUpdated = (
			(body !== post.body) ||
			(title !== post.title)
		);

		if(postUpdated) {
			await editPost(post._id, {
				body,
				title,
			});
		}

		const commentUpdated = parentPost?.comment !== parentReplyComment;

		if(parentPost && commentUpdated) {
			await connectPost(post._id, parentPost.parentPostId, 0, parentReplyComment);
		}

		await reload();

		setIsEditing(false);
		setBusy(false);
	}

	return (
		<Box
			sx={{ padding: 1 }}
			style={{ backgroundColor: '#f3f3f3' }}
		>
			<PostResponseModal
				open={responseModalIsOpen}
				post={post}
				onClose={() => setResponseModalIsOpen(false)}
			/>
			<div>
				{isEditing ? (
					<>
						{hasParent && (
							<TextField
								multiline
								fullWidth
								margin="dense"
								label="Response Comment (optional)"
								variant="outlined"
								minRows={3}
								disabled={busy}
								value={parentReplyComment}
								onChange={e => setParentReplyComment(e.target.value)}
							/>
						)}
					</>
				) : (
					<>
						{hasParentReply && (
							<>
								<Typography variant="subtitle1">
									<em>
								Top Comment: {parentPost.comment}
									</em>
								</Typography>
								<Button onClick={() => setResponseModalIsOpen(true)}>More</Button>
							</>
						)}
					</>
				)}
				<Box
					style={{
						cursor: 'pointer',
						fontWeight: 'bold',
					}}
				>
					<Link href={urlJoin(Paths.Post, post._id)} passHref>
						{isEditing ?
							(
								<TextField
									autoFocus
									fullWidth
									label="Title"
									variant="standard"
									disabled={busy}
									value={title}
									onChange={e => setTitle(e.target.value)}
								/>
							) : (
								<Typography
									component={MuiLink}
									noWrap
									variant="h5"
									title={post.title}
									style={{
										fontWeight: 'bold',
										display: 'block',
									}}
								>
									{post.title}
								</Typography>
							)}
					</Link>
				</Box>
				{isEditing ? (
					<TextField
						multiline
						fullWidth
						margin="dense"
						label="Body"
						variant="outlined"
						disabled={busy}
						minRows={6}
						value={body}
						onChange={e => setBody(e.target.value)}
					/>
				) : (
					<Typography
						variant="body1"
						sx={{ mb: 1.5 }}
						style={{
							fontSize: '20px',
							overflowWrap: 'break-word',
						}}
						dangerouslySetInnerHTML={{ __html: parseContentString(post.body) }}
					/>
				)}
				<Typography
					color="text.secondary"
					sx={{
						fontSize: 15,
						textAlign: 'right',
					}}
				>
					<Link href={urlJoin(Paths.Post, post._id)}>
						<a>
							<strong suppressHydrationWarning>
								{localizedDateFormat(post.created)}
							</strong>
						</a>
					</Link>
				</Typography>
				<Grid container alignItems="flex-end">
					<PostActions
						size="large"
						post={post}
					/>
					{isOwner && (
						<Grid
							xs
							item
							style={{ textAlign: 'center' }}
						>
							{isEditing ? (
								<ButtonGroup>
									<Tooltip title="Cancel">
										<IconButton
											key="cancel"
											color="error"
											size="large"
											disabled={busy}
											onClick={() => setIsEditing(false)}
										>
											<CancelIcon/>
										</IconButton>
									</Tooltip>

									<Tooltip title="Save">
										<IconButton
											key="save"
											color="success"
											size="large"
											onClick={save}
											disabled={busy || !isValid}
										>
											<ConfirmIcon/>
										</IconButton>
									</Tooltip>
								</ButtonGroup>
							) : (
								<Tooltip title="Edit">
									<IconButton
										key="edit"
										size="large"
										onClick={() => setIsEditing(true)}
									>
										<EditIcon/>
									</IconButton>
								</Tooltip>
							)}
						</Grid>
					)}
				</Grid>
			</div>
		</Box>
	);
}
