
import { useState } from 'react';
import Link from 'next/link';
import { ModalActions, NBSP } from '@common/constants';
import { useStore } from '@common/store';
import { useRouter } from 'next/router';
import { logout } from '@common/store-actions';
import { LoginIcon, LogoutIcon } from '@components/icons';
import {
	Avatar,
	IconButton,
	ListItemIcon,
	Menu,
	MenuItem,
	Typography,
} from '@mui/material';

export
function AccountMenu() {
	const user = useStore(s => s.user);
	const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
	const {
		pathname,
		query,
	} = useRouter();

	const isLoggedIn = !!user?.id;
	const open = !!anchorEl;

	function handleClick(event: React.MouseEvent<HTMLElement>) {
		setAnchorEl(event.currentTarget);
	}
	function handleClose() {
		setAnchorEl(null);
	}

	return (
		<>
			<IconButton onClick={handleClick}>
				<Avatar sx={{
					width: 32,
					height: 32,
				}} />
			</IconButton>
			<Menu
				disableScrollLock
				open={open}
				anchorEl={anchorEl}
				id="account-menu"
				onClose={handleClose}
				onClick={handleClose}
				PaperProps={{
					elevation: 0,
					sx: {
						overflow: 'visible',
						filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
						mt: 1.5,
						'& .MuiAvatar-root': {
							width: 32,
							height: 32,
							ml: -0.5,
							mr: 1,
						},
						'&:before': {
							content: '""',
							display: 'block',
							position: 'absolute',
							top: 0,
							right: 14,
							width: 10,
							height: 10,
							bgcolor: 'background.paper',
							transform: 'translateY(-50%) rotate(45deg)',
							zIndex: 0,
						},
					},
				}}
				transformOrigin={{
					horizontal: 'right',
					vertical: 'top',
				}}
				anchorOrigin={{
					horizontal: 'right',
					vertical: 'bottom',
				}}
			>
				{isLoggedIn && (
					[
						<MenuItem key="5">
							Signed in as{NBSP}
							<em>
								{user.email}
							</em>
						</MenuItem>,
						<MenuItem key="5.5" onClick={logout}>
							<ListItemIcon>
								<LogoutIcon fontSize="small" />
							</ListItemIcon>
							<Typography>
								Logout
							</Typography>
						</MenuItem>,
					]
				)}
				{!isLoggedIn && (
					<Link
						shallow
						passHref
						href={{
							pathname,
							query: {
								a: ModalActions.Login,
								...query,
							},
						}}
					>
						<MenuItem color="inherit">
							<ListItemIcon>
								<LoginIcon fontSize="small"/>
							</ListItemIcon>
							Login
						</MenuItem>
					</Link>
				)}
			</Menu>
		</>
	);
}
