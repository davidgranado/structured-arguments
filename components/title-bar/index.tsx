import Link from 'next/link';
import { LoginModal } from '@components/modals/login.modal';
import { AccountMenu } from './account-menu';
import { useStore } from '@common/store';
import { formatCompactNumber } from '@common/utils';
import { BackIcon, MenuIcon } from '@components/icons';
import {
	AppBar,
	Box,
	Chip,
	IconButton,
	Toolbar,
	Typography,
} from '@mui/material';

interface Props {
	showBack?: boolean;
}

export
function TitleBar(props: Props) {
	const { showBack } = props;
	const user = useStore(s => s.user);

	return (
		<>
			<Box sx={{
				marginBottom: 1,
				flexGrow: 1,
			}}>
				<AppBar position="static">
					<Toolbar>
						{showBack && (
							<Link href="/" passHref>
								<IconButton
									size="large"
									edge="start"
									color="inherit"
									sx={{ mr: 2 }}
								>
									<BackIcon />
								</IconButton>
							</Link>
						)}
						{!showBack && (
							<IconButton
								size="large"
								edge="start"
								color="inherit"
								sx={{ mr: 2 }}
							>
								<MenuIcon />
							</IconButton>
						)}
						<Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
							Structured Args
						</Typography>
						{!!user?.pointBalance && (
							<Chip size="small" color="success" label={`${formatCompactNumber(user?.pointBalance)} pts`} />
						)}
						<AccountMenu/>
					</Toolbar>
				</AppBar>
			</Box>
			<LoginModal/>
		</>
	);
}
