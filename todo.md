# Done
	- Post creation
	- Post respons

- Post Editing
	- Setup ownership relationship
	- Add abiity to break relationship with post
	- Save each edit as a revision

- Response Editing
	- Save each edit as a revision

- Post Editing
	- Add abiity to break relationship with post

# Todo

- Response Editing
	- Add abiity to break relationship with response

- Points
	- award system
	- usage system
	- possibly implemented as individual documents recording who has contributed?
	- Possibly implemented as object with user id keys and point value

- Surface post edit history

# Cleanup
- Make api less crappy
- Standardize response envelope to better propagate success/fail and messaging.
