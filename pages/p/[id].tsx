import Link from 'next/link';
import { withAuthUser } from 'next-firebase-auth';
import { Post } from '@common/types';
import { FeedItem } from '@components/feed-item';
import { useRouter } from 'next/router';
import { FocusedItem } from '@components/focused-item';
import { ActivityTypes, AUTH_USER_TOKENS_COOKIE_NAME } from '@common/constants';
import { last } from '@common/utils';
import type { GetServerSideProps, NextPage } from 'next';
import {
	fetchItem,
	parseJwt,
	recordActivity,
} from '@common/server/data-fns';
import {
	Tab,
	Tabs,
	Typography,
} from '@mui/material';
import { Layout } from '@components/layout';

interface Props {
	post: Post | null;
	related: Post[];
	parentPosts: Post[];
}

export
const getServerSideProps: GetServerSideProps<Props> = async (ctx) => {
	const user = await parseJwt(ctx.req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);
	const userId = user?.id || '';

	const {
		post,
		related,
		parentPosts,
	} = await fetchItem(ctx.params?.id as string || '', userId);

	recordActivity(userId, ActivityTypes.FeedView);

	return {
		props: {
			post,
			related,
			parentPosts,
		},
	};
};

const PostPage: NextPage<Props> = (props) => {
	const {
		pathname,
		query,
	} = useRouter();
	const {
		post,
		parentPosts,
		related,
	} = props;
	const { t: tab  = 'replies' } = query;
	const activeRelatedIds = tab === 'replies-to' ?
		post?.parentPostResponses.map(r => r.parentPostId) :
		post?.childPostIds;

	const activeRelated = related.filter(r => r._id && activeRelatedIds?.includes(r._id));

	return (
		<Layout title={post?.title}>
			{parentPosts.map((p, i) => (
				<FeedItem
					key={p._id}
					post={p}
					parentId={parentPosts[i - 1]?._id || ''}
				/>
			))}
			{post && (
				<FocusedItem post={post} parentId={last(parentPosts)?._id || ''} />
			)}
			<Tabs centered value={tab}>
				{/** @ts-ignore for "value" prop being passed to tab, which can't be set directly for some reason*/}
				<Link value="replies"
					passHref
					replace
					href={{
						pathname,
						query: {
							...query,
							t: 'replies',
						},
					}}
				>
					<Tab LinkComponent="a" label="Replies" />
				</Link>
				{/** @ts-ignore for "value" prop being passed to tab, which can't be set directly for some reason*/}
				<Link value="replies-to"
					passHref
					replace
					href={{
						pathname,
						query: {
							...query,
							t: 'replies-to',
						},
					}}
				>
					<Tab LinkComponent="a" label="Replies To" />
				</Link>
			</Tabs>
			{!!activeRelated.length && activeRelated.map(ar => (
				<FeedItem
					key={ar._id}
					post={ar}
					parentId={post?._id}
				/>
			))}
			{!activeRelated.length && (
				<Typography>
					{tab === 'replies-to' ?
						'Does not reply to any posts' :
						'No Replies'}
				</Typography>
			)}
		</Layout>
	);
};

export default withAuthUser<Props>()(PostPage);
