import { withAuthUser } from 'next-firebase-auth';
import { FeedItem } from '@components/feed-item';
import { useStore } from '@common/store';
import { State } from '@common/types';
import { ActivityTypes, AUTH_USER_TOKENS_COOKIE_NAME } from '@common/constants';
import { isServerReq } from '@common/server/utils';
import { Layout } from '@components/layout';
import { HeadingTabs } from '@components/heading-tabs';
import { SearchField } from '@components/search-field';
import type { GetServerSideProps, NextPage } from 'next';
import {
	fetchFeed,
	getPointBalance,
	parseJwt,
	recordActivity,
} from '@common/server/data-fns';

interface Props {
	initialState: Partial<State>;
}

export
const getServerSideProps: GetServerSideProps<Props> = async (ctx) => {
	const user = await parseJwt(ctx.req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);
	const userId = user?.id || '';

	recordActivity(userId, ActivityTypes.FeedView);

	const feed = await fetchFeed(userId);
	const initialState: Partial<State> = { feed };

	if(user?.email && user.id && isServerReq(ctx.req)) {
		initialState.user = {
			email: user.email,
			id: user.id,
			pointBalance: await getPointBalance(user.id),
		};
	}

	return { props: { initialState } };
};

const HomePage: NextPage<Props> = () => {
	const feed = useStore(s => s.feed);

	return (
		<Layout>
			<SearchField/>
			<HeadingTabs/>
			{feed.map(p => (
				<FeedItem key={p._id} post={p}/>
			))}
		</Layout>
	);
};

export default withAuthUser<Props>()(HomePage);
