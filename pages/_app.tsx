import type { AppProps } from 'next/app';
import { useEffect } from 'react';
import Head from 'next/head';
import { init, refreshPointBalance } from '@common/store-actions';
import { State } from '@common/types';
import { Toast } from '@common/toast';
import { useAuthUser, withAuthUser } from 'next-firebase-auth';
import { LoadingOverlay } from '@components/loading-overlay';
import {
	Provider,
	useCreateStore,
	useStore,
} from '@common/store';

import '@styles/globals.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

interface Props {
	initialState: Partial<State>;
}

function MyApp(props: AppProps<Props>) {
	const {
		Component,
		pageProps,
	} = props;

	const createStore = useCreateStore(pageProps.initialState);

	useEffect(() => {
		init();
	}, []);

	return (
		<>
			<Head>
				<meta name="viewport" content="initial-scale=1, width=device-width" />
			</Head>
			<Provider createStore={createStore}>
				<Component {...pageProps} />
				<Toast/>
				<LoadingOverlay/>
				<Foo/>
			</Provider>
		</>);
}

// TODO where should this go?
const Foo = withAuthUser()(() => {
	const authUser = useAuthUser();
	const user = useStore(s => s.user);

	useEffect(() => {
		if(authUser.id && !user?.pointBalance) {
			// TODO Fix this race condition and figure out how to wait until after
			// next-firebase-auth finished setup like setting cookies n' stuff
			setTimeout(() => {
				refreshPointBalance();
			}, 1000);
		}
	}, [authUser.id, user?.id]);

	return <>{null}</>;
});

export default MyApp;
