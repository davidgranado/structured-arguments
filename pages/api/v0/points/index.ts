// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { AUTH_USER_TOKENS_COOKIE_NAME } from '@common/constants';
import { getPointBalance, parseJwt } from '@common/server/data-fns';
import { initAuth } from '@common/server/utils';

initAuth();

export default
async function handler(req: NextApiRequest, res: NextApiResponse<any>) {
	const user = await parseJwt(req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);

	if(!user?.id) {
		return res.status(401).json({
			ok: false,
			data: null,
		});
	}

	try {
		res.send({
			ok: true,
			data: { points: await getPointBalance(user.id) },
		});
	} catch(e) {
		console.error(e);
		return res
			.status(500)
			.json(e);
	}
}
