// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { AUTH_USER_TOKENS_COOKIE_NAME, DbCollections } from '@common/constants';
import { getPointBalance, parseJwt } from '@common/server/data-fns';
import { initAuth } from '@common/server/utils';
import { dbClientPromise } from '@common/server/mongodb';
import { DbPointTransaction } from '@common/server/db-schema';
import { nowISOString } from '@common/utils';
import Joi from 'joi';

initAuth();

interface Schema {
	points: number;
	userId: string;
}

const schema = Joi.object<Schema>({
	userId: Joi
		.string()
		.required(),
	points: Joi
		.number()
		.positive()
		.required(),
});

export default
async function handler(req: NextApiRequest, res: NextApiResponse<any>) {
	const user = await parseJwt(req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);

	if(!user?.claims.admin) {
		return res.status(401).json({ ok: false });
	}

	try {
		const {
			points,
			userId,
		}: Schema = await schema.validateAsync(req.body || req.query);

		const newTransaction = await addPoints(userId, points);

		res.send({ newTransaction });
	} catch(e) {
		console.error(e);
		return res
			.status(500)
			.json(e);
	}
}

async function addPoints(userId: string, points: number) {
	const prevBalance = await getPointBalance(userId);

	const db = await dbClientPromise;
	const postTransactions = db.collection<DbPointTransaction>(DbCollections.PointTransactions);

	const newTransaction = {
		points,
		date: nowISOString(),
		userId,
		balance: points + prevBalance,
	};

	await postTransactions.insertOne(newTransaction);

	return newTransaction;
}
