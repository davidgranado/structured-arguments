import { initAuth } from '@common/server/utils';
import { dbClientPromise } from '@common/server/mongodb';
import { ObjectId } from 'mongodb';
import { NextApiRequest, NextApiResponse } from 'next';
import { parseJwt } from '@common/server/data-fns';
import Joi from 'joi';
import { nowISOString } from '@common/utils';
import * as validations from '@common/server/validations';
import {
	AUTH_USER_TOKENS_COOKIE_NAME,
	DbCollections,
	POST_CREATE_POINT_RATIO,
} from '@common/constants';
import type {
	DbPointTransaction,
	DbPost,
	DbPostOwner,
	DbPostPoints,
} from '@common/server/db-schema';

initAuth();

interface Schema {
	points: number;
	comment?: {
		commentUserId: string;
		parentPostId: string;
	}
}

const schema = Joi.object<Schema>({
	points: Joi.number()
		.integer()
		.required(),
	comment: Joi.object({
		commentUserId: validations
			.userId
			.required(),
		parentPostId: validations
			.mongoId
			.required(),
	}).optional(),
});

const FOO = {
	commentUserId: '',
	parentPostId: '',
};

export default
async function handler(req: NextApiRequest, res: NextApiResponse) {
	try {
		const user = await parseJwt(req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);

		if(!user?.id) {
			return res
				.status(401)
				.send({
					ok: false,
					msg: 'Not logged in',
				});
		}

		const id = req.query.id as string;
		const {
			points,
			comment: {
				commentUserId,
				parentPostId,
			} = FOO,
		}: Schema = await schema.validateAsync(req.body);

		console.log(111, commentUserId, parentPostId);

		await boostPost(id, user.id, points, commentUserId, parentPostId);

		return res
			.status(200)
			.json({ ok: true });
	} catch (e) {
		console.error(e);
		return res
			.status(500)
			.json(e);
	}
}

// TODO Is there a better way to do this in MongoDB

async function boostPost(
	postId: string,
	userId: string,
	points: number,
	commentUserId?: string,
	parentPostId?: string,
) {
	const date = nowISOString();
	const postIdObj = new ObjectId(postId);
	const db = await dbClientPromise;

	const isOwner = !!await db.collection<DbPostOwner>(DbCollections.PostOwners).findOne({
		postId: postIdObj,
		userId,
	});

	const appliedPoints = isOwner ? points * POST_CREATE_POINT_RATIO : points;
	const postFilter: any = { _id: postIdObj };
	const postIncUpdate: any = { $inc: { points: appliedPoints } };
	let postPointsComment = undefined;

	console.log(222, commentUserId, parentPostId);

	if(commentUserId && parentPostId) {
		const isOwnerComment = commentUserId === userId;
		postFilter['parentPostResponses.userId'] = commentUserId;
		postFilter['parentPostResponses.parentPostId'] = parentPostId;
		postIncUpdate.$inc['parentPostResponses.$.points'] = isOwnerComment ? points * POST_CREATE_POINT_RATIO : points;
		postPointsComment = {
			commentUserId,
			parentPostId,
		};
	}

	const dbCalls = [
		db.collection<DbPost>(DbCollections.Posts)
			.updateOne(
				postFilter,
				postIncUpdate
			),
		db.collection<DbPostPoints>(DbCollections.PostPoints)
			.insertOne({
				date,
				userId,
				postId: postIdObj,
				points: appliedPoints,
				comment: postPointsComment,
			}),
		db.collection<DbPointTransaction>(DbCollections.PointTransactions)
			.aggregate([
				{ $match: { userId } },
				{ $sort: { date: -1 } },
				{ $limit: 1 },
				{
					$project: {
						_id: new ObjectId(),
						points,
						date,
						userId: '$userId',
						balance: { $add: ['$balance', -points] },
					},
				},
				{ $merge: { into: DbCollections.PointTransactions } },
			])
			.toArray(),
	];

	await Promise.all(dbCalls);
}
