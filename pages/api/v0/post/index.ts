import { initAuth } from '@common/server/utils';
import { dbClientPromise } from '@common/server/mongodb';
import { ObjectId } from 'mongodb';
import {
	AUTH_USER_TOKENS_COOKIE_NAME,
	DbCollections,
	POST_CREATE_POINT_RATIO,
} from '@common/constants';
import { escapeHtml, nowISOString } from '@common/utils';
import { NextApiRequest, NextApiResponse } from 'next';
import { NewPost, Post } from '@common/types';
import { parseJwt, spendPoints } from '@common/server/data-fns';
import {
	DbPost,
	DbPostOwner,
	DbPostPoints,
} from '@common/server/db-schema';
import Joi from 'joi';
import * as validations from '@common/server/validations';

initAuth();

interface Schema {
	points: number;
	post: NewPost;
}

const schema = Joi.object<Schema>({
	points: validations.points.required(),
	post: validations.newPostResponse.required(),
});

export default
async function handler(req: NextApiRequest, res: NextApiResponse) {
	try {
		const user = await parseJwt(req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);

		if(!user?.id) {
			return res
				.status(401)
				.send({
					ok: false,
					msg: 'Not logged in',
				});
		}

		const {
			points,
			post,
		} = await schema.validateAsync(req.body);

		return res
			.status(200)
			.json({
				ok: true,
				post: await createPost(post, points, user.id),
			});
	} catch (error) {
		console.error(error);
		return res
			.status(500)
			.json({
				ok: false,
				error,
			});
	}
}

async function createPost(post: NewPost, points: number, userId: string): Promise<Post> {
	const created = nowISOString();
	const spentPoints = points * POST_CREATE_POINT_RATIO;
	const newPost: DbPost = {
		body: escapeHtml(post.body),
		created,
		lastUpdated: created,
		ownerId: userId,
		childPostIds: [],
		title: post.title,
		points: spentPoints,
		parentPostResponses: post.parentPostResponses.map(r => ({
			comment: r.comment && escapeHtml(r.comment),
			points: spentPoints,
			userId,
			created,
			parentPostId: r.parentPostId,
			lastUpdated: created,
		})),
	};

	await spendPoints(userId, points);

	const db = await dbClientPromise;
	const posts = db.collection<DbPost>(DbCollections.Posts);
	const postOwners = db.collection<DbPostOwner>(DbCollections.PostOwners);
	const postPoints = db.collection<DbPostPoints>(DbCollections.PostPoints);

	const { insertedId: _id } = await posts.insertOne(newPost);

	const dbCalls = [
		postPoints.insertOne({
			userId: userId,
			postId: _id,
			date: created,
			points: spentPoints,
		}),
		postOwners.insertOne({
			userId: userId,
			postId: _id,
		}),
	];

	if(post.parentPostResponses.length) {
		dbCalls.push(
			// @ts-ignore figure out type error on insertion
			posts.updateMany({ _id: { $in: post.parentPostResponses.map(r => new ObjectId(r.parentPostId)) } }, { $push: { childPostIds: _id.toString() } }),
		);
	}

	await Promise.all(dbCalls);

	return {
		...newPost,
		_id: _id.toString(),
	};
}
