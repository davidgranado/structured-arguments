import { initAuth } from '@common/server/utils';
import { parseJwt } from '@common/server/data-fns';
import Joi from 'joi';
import * as v from '@common/server/validations';
import type { NextApiRequest, NextApiResponse } from 'next';
import { AUTH_USER_TOKENS_COOKIE_NAME } from '@common/constants';
import { responseUpdate } from '@common/server/data-fns/response-update';

initAuth();

interface Schema {
	comment: string;
	id?: string;
	parentId?: string;
	points: number;
}

const schema = Joi.object<Schema>({
	comment: v.postComment.required(),
	id: v.mongoId.required(),
	parentId: v.mongoId.required(),
	points: v.points.required(),
});

export default
async function handler(req: NextApiRequest, res: NextApiResponse) {
	try {
		const user = await parseJwt(req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);

		if(!user?.id) {
			return res
				.status(401)
				.send({
					ok: false,
					msg: 'Not logged in',
				});
		}

		const {
			comment,
			points,
			id,
			parentId,
		} = await schema.validateAsync({
			...req.query,
			...req.body,
		});

		if(!(id && parentId)) {
			return res
				.status(400)
				.send({
					ok: false,
					msg: 'Missing required parameters.',
				});
		}

		return res
			.status(200)
			.json({
				ok: true,
				post: await responseUpdate({
					comment,
					parentPostId: parentId,
					points,
					postId: id,
					userId: user.id,
				}),
			});
	} catch (message) {
		console.error(message);
		return res
			.status(500)
			.json({
				ok: false,
				message,
			});
	}
}
