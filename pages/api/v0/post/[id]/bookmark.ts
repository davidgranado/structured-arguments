import { initAuth } from '@common/server/utils';
import { dbClientPromise } from '@common/server/mongodb';
import { AUTH_USER_TOKENS_COOKIE_NAME, DbCollections } from '@common/constants';
import { parseJwt } from '@common/server/data-fns';
import { DbBookmark } from '@common/server/db-schema';
import { ObjectId } from 'mongodb';
import { nowISOString } from '@common/utils';
import type { NextApiRequest, NextApiResponse } from 'next';

initAuth();

export default
async function handler(req: NextApiRequest, res: NextApiResponse) {
	const postId = req.query.id as string;

	try {
		const user = await parseJwt(req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);

		if(!user?.id) {
			return res
				.status(401)
				.send({
					ok: false,
					msg: 'Not logged in',
				});
		}

		const db = await dbClientPromise;
		const postObjId = new ObjectId(postId);

		await db
			.collection<DbBookmark>(DbCollections.PostBookmarks)
			.updateOne(
				{
					userId: user.id,
					postId: postObjId,
				},
				{
					$setOnInsert: {
						userId: user.id,
						postId: postObjId,
						date: nowISOString(),
					},
				},
				{ upsert: true }
			);


		return res
			.status(200)
			.json({ ok: true });
	} catch (message) {
		console.error(message);
		return res
			.status(500)
			.json({
				ok: false,
				message,
			});
	}
}
