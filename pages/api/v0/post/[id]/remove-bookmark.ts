import { initAuth } from '@common/server/utils';
import { dbClientPromise } from '@common/server/mongodb';
import { AUTH_USER_TOKENS_COOKIE_NAME, DbCollections } from '@common/constants';
import { parseJwt } from '@common/server/data-fns';
import type { NextApiRequest, NextApiResponse } from 'next';
import type { DbBookmark } from '@common/server/db-schema';
import { ObjectId } from 'mongodb';

initAuth();

export default
async function handler(req: NextApiRequest, res: NextApiResponse) {
	const postId = req.query.id as string;

	try {
		const user = await parseJwt(req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);

		if(!user?.id) {
			return res
				.status(401)
				.send({
					ok: false,
					msg: 'Not logged in',
				});
		}

		const db = await dbClientPromise;

		db
			.collection<DbBookmark>(DbCollections.PostBookmarks)
			.deleteOne(
				{
					userId: user.id,
					postId: new ObjectId(postId),
				},
			);


		return res
			.status(200)
			.json({ ok: true });
	} catch (message) {
		console.error(message);
		return res
			.status(500)
			.json({
				ok: false,
				message,
			});
	}
}
