import { initAuth } from '@common/server/utils';
import { dbClientPromise } from '@common/server/mongodb';
import { ObjectId } from 'mongodb';
import { NextApiRequest, NextApiResponse } from 'next';
import { AUTH_USER_TOKENS_COOKIE_NAME, DbCollections } from '@common/constants';
import { nowISOString, pick } from '@common/utils';
import { parseJwt } from '@common/server/data-fns';
import type { PostUpdate } from '@common/types';
import type { DbPost, DbPostHistory } from '@common/server/db-schema';

initAuth();

export default
async function handler(req: NextApiRequest, res: NextApiResponse) {
	try {
		const user = await parseJwt(req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);

		if(!user?.id) {
			return res
				.status(401)
				.send({
					ok: false,
					msg: 'Not logged in',
				});
		}

		const id = req.query.id as string;
		const updates: PostUpdate = req.body.postUpdate;

		const newPost = await updatePost(id, updates);

		return res
			.status(200)
			.json({
				ok: true,
				post: newPost,
			});
	} catch (e) {
		console.error(e);
		return res
			.status(500)
			.json(e);
	}
}

async function updatePost(id: string, postUpdates: PostUpdate) {
	const _id = new ObjectId(id);
	const lastUpdated = nowISOString();
	const db = await dbClientPromise;
	const posts = db.collection<DbPost>(DbCollections.Posts);
	const postEditHistory = await db.collection<DbPostHistory>(DbCollections.PostEditHistory);
	const originalPost = await posts.findOne<DbPost>({ _id });

	if(!originalPost) {
		throw 'Cannot find post to edit';
	}

	const dbCalls: Promise<any>[] = [
		posts.updateOne({ _id }, {
			$set: {
				...postUpdates,
				lastUpdated,
			},
		}),
	];

	if(originalPost.ownerId) {
		const newHistory: DbPostHistory = {
			ownerId: originalPost.ownerId,
			date: lastUpdated,
			postContent: pick(originalPost, 'body', 'title'),
		};

		dbCalls.push(
			postEditHistory.insertOne(newHistory as any)
		);
	}

	await Promise.all(dbCalls);
}
