import { AUTH_USER_TOKENS_COOKIE_NAME, DbCollections } from '@common/constants';
import { parseJwt } from '@common/server/data-fns';
import { dbClientPromise } from '@common/server/mongodb';
import { initAuth } from '@common/server/utils';
import { ObjectId } from 'mongodb';
import type { NextApiRequest, NextApiResponse } from 'next';
import type{ DbBookmark, DbPost } from '@common/server/db-schema';

initAuth();

export default
async function handler(req: NextApiRequest, res: NextApiResponse) {
	try {
		const db = await dbClientPromise;
		const id = req.query.id as string;

		if(!id) {
			throw 'No valid post id';
		}

		const post = await db
			.collection<DbPost>(DbCollections.Posts)
			.findOne({ _id: new ObjectId(id) });

		if(!post) {
			throw `Can't find post "${id}".`;
		}

		let bookmarked = false;

		const user = await parseJwt(req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);

		if(user?.id) {
			bookmarked = !!await db
				.collection<DbBookmark>(DbCollections.PostBookmarks)
				.findOne({
					userId: user.id,
					postId: new ObjectId(id),
				});
		}


		return res
			.status(200)
			.json({
				ok: true,
				post: {
					...post,
					bookmarked,
				},
			});
	} catch (e) {
		console.error(e);
		return res
			.status(500)
			.json(e);
	}
}
