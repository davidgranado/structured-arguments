// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import { AUTH_USER_TOKENS_COOKIE_NAME } from '@common/constants';
import { parseJwt } from '@common/server/data-fns';

export default
async function handler(req: NextApiRequest, res: NextApiResponse<any>) {
	const user = await parseJwt(req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);

	res.status(200).json({ user });
}
