// ./pages/api/logout
import { initAuth } from '@common/server/utils';
import { NextApiRequest, NextApiResponse } from 'next';
import { unsetAuthCookies } from 'next-firebase-auth';

initAuth();

export default
async function handler(req: NextApiRequest, res: NextApiResponse<any>) {
	try {
		await unsetAuthCookies(req, res);
	} catch (e) {
		return res.status(500).json({ error: 'Unexpected error.' });
	}
	return res.status(200).json({ ok: true });
}
