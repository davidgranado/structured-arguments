import { withAuthUser } from 'next-firebase-auth';
import { FeedItem } from '@components/feed-item';
import { Post, State } from '@common/types';
import { ActivityTypes, AUTH_USER_TOKENS_COOKIE_NAME } from '@common/constants';
import { isServerReq } from '@common/server/utils';
import { Layout } from '@components/layout';
import { SearchField } from '@components/search-field';
import type { GetServerSideProps, NextPage } from 'next';
import {
	fetchUserPosts,
	getPointBalance,
	parseJwt,
	recordActivity,
} from '@common/server/data-fns';

interface Props {
	posts: Post[];
	initialState?: Partial<State>;
}

export
const getServerSideProps: GetServerSideProps<Props> = async (ctx) => {
	const user = await parseJwt(ctx.req.cookies[AUTH_USER_TOKENS_COOKIE_NAME]);
	const userId = user?.id || '';

	if(!(user?.email && user?.id)) {
		return { props: { posts: [] } };
	}

	recordActivity(userId, ActivityTypes.BookmarkListView);

	const initialState: Partial<State> = { };

	if(isServerReq(ctx.req)) {
		initialState.user = {
			email: user.email,
			id: user.id,
			pointBalance: await getPointBalance(user.id),
		};
	}

	return {
		props: {
			posts: await fetchUserPosts(userId),
			initialState,
		},
	};
};

const UserBoostPage: NextPage<Props> = (props) => {
	const { posts } = props;

	return (
		<Layout title="My Boosts">
			<SearchField/>
			{posts.map(p => (
				<FeedItem key={p._id} post={p}/>
			))}
		</Layout>
	);
};

export default withAuthUser<Props>()(UserBoostPage);
